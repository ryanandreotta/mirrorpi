/**
 * Created by Ryan on 1/8/2016.
 */
'use strict';

var hapi = require('hapi');
var rabbit = require('rabbit-pub-sub');

var esbConfig = {
    server: "localhost",
    queue: "mirror-queue",
    exchange: "mirror-exchange"
};

var logger = {
    info: function(str) {
        console.log(str);
    },
    error: function(str){
        console.log(str);
    }
};


var server = new hapi.Server();

server.connection({ port: 80 });

server.route({
    method: 'GET',
    path: '/updateMessage/{newMessage}',
    handler: updateMessage
});

server.start(function(err){
    if(err){
        throw err;
    }
    console.log('server running at ' + server.info.uri);
});

function updateMessage(request, reply){

    //get the message
    var message = request.params.newMessage;
    console.log('message received: ' + message);

    //put the message on the esb
    new rabbit.MessagePublisher(esbConfig, logger, function(publisher) {
        publisher.publish({message: message});
        publisher.end(); // Not doing this will leak connections!
        console.log('message published');
    });

    reply('Way to go');
}