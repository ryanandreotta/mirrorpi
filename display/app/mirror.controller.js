(function(){
    'use strict';

    angular
        .module('mirrorMirror')
        .controller('mirrorController', mirrorController);

    mirrorController.$inject = ['socketService', '$interval', 'weatherService'];

    function mirrorController(socketService, $interval, weatherService){

        var vm = this;
        var today = moment();

        vm.bottomContent = 'Hello Rusty!';
        vm.currentTemperature = 99;
        vm.dayName = today.format('dddd');
        vm.dayNumber = today.format('Do');
        vm.highTemperature = 99;
        vm.monthName = today.format('MMMM');
        vm.weatherIconClass = '';
        vm.imageUrl = 'images/IMG_2304-ANIMATION.gif';

        activate();

        function activate(){
            loadWeather();
        }

        function loadWeather(){
            weatherService.getWeather().then(function(data){
                vm.currentTemperature = Math.round(data.main.temp);
                vm.highTemperature = Math.round(data.main.temp_max);

                if(data.weather[0].description === 'clear sky'){
                    vm.weatherIconClass = 'wi-day-sunny';
                }else if(data.weather[0].description === 'few clouds'){
                    vm.weatherIconClass = 'wi-day-cloudy';
                }else if(data.weather[0].description === 'scattered clouds'){
                    vm.weatherIconClass = 'wi-day-cloudy';
                }else if(data.weather[0].description === 'overcast clouds'){
                    vm.weatherIconClass = 'wi-cloudy';
                }else if(data.weather[0].description === 'broken clouds'){
                    vm.weatherIconClass = 'wi-day-cloudy';
                }else if(data.weather[0].description === 'shower rain'){
                    vm.weatherIconClass = 'wi-day-sprinkle';
                }else if(data.weather[0].description.indexOf('rain') > -1){
                    vm.weatherIconClass = 'wi-day-rain';
                }else if(data.weather[0].description === 'thunderstorm'){
                    vm.weatherIconClass = 'wi-day-lightning';
                }else if(data.weather[0].description.indexOf('snow') > -1){
                    vm.weatherIconClass = 'wi-day-snow';
                }else if(data.weather[0].description === 'mist'){
                    vm.weatherIconClass = 'wi-day-fog';
                }


            }, function(err){

            });
        }

        $interval(function(){
            var now = moment();
            vm.dayName = now.format('dddd');
            vm.dayNumber = now.format('Do');
            vm.monthName = now.format('MMMM');

            loadWeather();

        }, 300000);

        $interval(function(){
            vm.imageUrl = 'http://www.kickstandit.com/image.jpg' + '?' + new Date().getTime();
        }, 900000);

        socketService.on('bottomContent', function(content){
            vm.bottomContent = content;
        });
    }
})();
