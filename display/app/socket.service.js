(function(){
    'use strict';

    angular.module('mirrorMirror').factory('socketService', socketService);

    socketService.$inject = ['socketFactory'];

    function socketService(socketFactory){
        return socketFactory();
    }
})();
