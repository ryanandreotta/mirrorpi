(function(){
    'use strict';

    angular.module('mirrorMirror').factory('weatherService', weatherService);

    weatherService.$inject = ['$resource'];

    function weatherService($resource){

        return {
            getWeather : getWeather
        };

        function getWeather(){
            var weather = $resource('http://api.openweathermap.org/data/2.5/weather?zip=14609,us&appid=2eeb26195ef8964a9a62330ebaf1eff8&units=Imperial');
            return weather.get().$promise;
        }
    }
})();