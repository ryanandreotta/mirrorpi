'use strict';

var hapi = require('hapi');
var server = new hapi.Server();
var exec = require('child_process').exec;
var monitorState = 'on';

process.chdir('/var/www/display');

server.connection({port: 80});
var io = require('socket.io')(server.listener);
var mySocket;

io.on('connection', function (socket) {
    console.log('connection');
    mySocket = socket;
});

server.register(require('inert'), (err) => {
    if (err) {
        throw err;
    }

    server.route({
        method: 'GET',
        path: '/',
        handler: function (request, reply) {
            reply.file('./index.html');
        }
    });

    server.route({
        method: 'GET',
        path: '/app/{param*}',
        handler: {
            directory: {
                path: 'app'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/adminApp/{param*}',
        handler: {
            directory: {
                path: 'adminApp'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/bower_components/{param*}',
        handler: {
            directory: {
                path: 'bower_components'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/css/{param*}',
        handler: {
            directory: {
                path: 'css'
            }
        }
    });

    server.route({
        method: 'GET',
        path: '/images/{param*}',
        handler: {
            directory: {
                path: 'images'
            }
        }
    });

    server.route({
       method: 'GET',
        path: '/mirrorAdminXYZ',
        handler: function (request, reply) {
            reply.file('./update.html');
        }

    });

    server.route({
        method: 'GET',
        path: '/rebootTheMirrorNowXYZ',
        handler: reboot

    });

    server.route({
        method: 'GET',
        path: '/updateBottomContent/{newContent}',
        handler: updateBottomContent
    });

    // server.route({
    //    method: 'GET',
    //     path: '/turnOff',
    //     handler: turnOff
    // });
    //
    // server.route({
    //    method: 'GET',
    //     path: '/turnOn',
    //     handler: turnOn
    // });
});

//start listening for requests
server.start(function (err) {
    if (err) {
        throw err;
    }
    console.log('Display server running at ' + server.info.uri);
});

setInterval(checkTime, 10000);

function reboot(request, reply){
    var reboot = exec('sudo reboot', function(err, stdout, stderr){
        console.log('rebooted');
    });

    return reply(null, null);
}
function turnOffMonitor(){
    var turnOn = exec('sudo /opt/vc/bin/tvservice -o', function(err, stdout, stderr){
        console.log('powered off');
    });

    monitorState = 'off';
}

function turnOnMonitor(){
    console.log('powering on');
    exec('sudo /opt/vc/bin/tvservice -p', function(err, stdout, stderr){
        exec('sudo chvt 1', function(err, stdout, stderr){
            exec('sudo chvt 7', function(err, stdout, stderr){
                console.log('powered on');
            });
        });
    });

    monitorState = 'on';
}

function turnOff(request, reply){
    turnOffMonitor();
    return reply(null, null);
}

function turnOn(request, reply){
    turnOnMonitor();
    return reply(null, null);
}

function updateBottomContent(request, reply){

    var message = request.params.newContent;
    console.log('message received: ' + message);

    if(mySocket){
        mySocket.emit('bottomContent', message);
        console.log('emitted!');
    }

    return reply(null, null);
}

function checkTime(){
    var hours = new Date().getHours();
    var minutes = new Date().getMinutes();

    if(hours >= 19 && minutes >= 30 && monitorState === 'on'){
        turnOffMonitor();
    }

    if(hours === 7 && monitorState === 'off'){
        turnOnMonitor();
    }
}
