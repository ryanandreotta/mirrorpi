(function(){
    'use strict';

    angular.module('mirrorAdmin').controller('adminController', adminController);

    adminController.$inject = ['$http', '$location'];

    function adminController($http, $location){

        var vm = this;

        vm.bottomText = '';
        vm.showIp = true;
        vm.ip = $location.host();

        vm.turnOn = turnOn;
        vm.turnOff = turnOff;

        vm.updateText = updateText;

        function turnOn(){
            $http.get('http://' + vm.ip + '/turnOn');
        }

        function turnOff(){
            $http.get('http://' + vm.ip + '/turnOff');
        }

        function updateText(){
            $http.get('http://' + vm.ip + '/updateBottomContent/' + vm.bottomText);
        }

    }

})();